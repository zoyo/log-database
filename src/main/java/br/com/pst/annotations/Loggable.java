package br.com.pst.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import br.com.pst.model.ObjectLog;

/**
 * Informa para o framework que as altera��s referentes a essa entidade devem ser logadas.<br>
 * A presen�a dessa anota��o na classe n�o garante que ser� gerado log, � necess�rio tamb�m 
 * que os atributos que devem ser logados estejam anotados com {@link LogField}.<br><br>
 * 
 * A anota��o permite configurar o nome da tabela que ser� enviada para o atributo <code>'Nome Tabela'</code> 
 * de {@link ObjectLog} atrav�s do atributo <code>tableName</code> que, ser n�o for informado, 
 * ser� usado o nome da classe.<br><br>
 * 
 * Exige o preenchimento do atributo <code>'identity'</code> que identificar� na classe anotada, quem ser� 
 * o identificador (tip chave prim�ria) do log. O atributo aqui informado, ser� lido e seu conte�do ser� 
 * enviado para o campo <code>'ID Registro'</code> do {@link ObjectLog}.<br>
 * N�o se preocupe se sua entidade possui um identificador composto, pois o framework usa o <code>'toString'</code>
 * para recuperar o conte�do, ent�o, basta que o mesmo esteja implementado corretamente.
 * 
 * @param tableName Nome da tabela que ser� enviada ao atribuo <code>'Nome Tabela'</code> de {@link ObjectLog}.
 * @param identity Nome do atributo (case-sensitive) que representa o indentificador (tipo chave prim�ria) do objeto log�vel.
 * 
 * @author rlbatista
 *
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface Loggable {
	
	/**
	 * Nome da tabela que ser� enviado ao atributo <code>'Nome Tabela'</code> de {@link ObjectLog}.
	 */
	String tableName() default "";
	
	/**
	 * Nome do atributo que deve ser usado para identificar a qual registro, os dados de log pertencem,
	 * como por exemplo, a chave prim�ria da tabela alterada, permitindo rastreamento.
	 */
	String identity();
}
