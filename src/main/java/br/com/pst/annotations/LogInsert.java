package br.com.pst.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import br.com.pst.model.ObjectLog;

/**
 * Anota��o usada para definir o ponto de log da opera��o de inser��o no banco.<br>
 * 
 * O log grava um texto indicando qual opera��o foi realizada no objeto/entidade e esse texto � configur�vel 
 * atrav�s do atributo <code>describeOperation</code> (padr�o "INSERT").<br>
 * Ex:<br><br>
 * 
 * <code>
 * &#64;LogInsert(describeOperation="NOVO")<br>
 * </code>
 * 
 * @param describeOperation Texto que ser� enviado ao atributo <code>'Nome opera��o'</code> de {@link ObjectLog}.
 * 
 * @author rlbatista
 *
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface LogInsert {
	
	/**
	 * Define o texto que deve ser enviado para o atributo <code>'Nome opera��o'</code> do {@link ObjectLog}.
	 */
	String describeOperation() default "INSERT";
}
