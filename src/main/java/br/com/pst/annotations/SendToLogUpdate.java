package br.com.pst.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Essa anota��o deve ser usada no m�todo que retorna o objeto orginal. De maneira geral, ser� usada
 * no m�todo que recupera o objeto do banco de dados para edi��o. A atributo <code>putObjectIn</code> da anota��o 
 * serve para identificar o objeto para ser recuperado no momento do update, da seguinte forma:<br>
 * <ol>
 * 		<li>O framework intercepta o retorno do m�todo anotado com <code>&#64;SendToLogUpdate</code> (n�o anotar m�todos void);</li>
 * 		<li>O objeto capturado � armazenado sob a identifica��o informada no atributo <code>putObjectIn</code>;</li>
 * 		<li>No momento do update, o framework busca o objeto capturado (ver {@link LogUpdate#getObjectFrom()}) para realizar a compara��o com o objeto atual;</li>
 * 		<li>O framework captura as diferen�as;</li>
 * </ol>	 
 * 
 * @param putObjectIn Nome do identificador do objeto orinal que {@link LogUpdate#getObjectFrom()} usar� para resgata-lo.
 * 
 * @author rlbatista
 *
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface SendToLogUpdate {
	
	/**
	 * Identificador que ser� usado por {@link LogUpdate#getObjectFrom()} para resgatar o objeto original.<br>
	 * Use um identificador diferente para cada entidade que deseja logar.
	 */
	String putObjectIn();
}
