package br.com.pst.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import br.com.pst.model.ObjectLog;

/**
 * Anota��o usada para definir o ponto de log da opera��o de update no banco.<br><br>
 * 
 * É muito importante o atributo <code>getObjectFrom</code>, pois � usado para recupera��o do objeto original
 * e tem que estar em sincronia com o {@link SendToLogUpdate#putObjectIn()} do objeto sendo logado.<br><br>
 * 
 * O log grava um texto indicando qual opera��o foi realizada no objeto/entidade e esse texto � configur�vel 
 * atrav�s do atributo <code>describeOperation</code> (padr�o "UPDATE").<br>
 * Ex:<br><br>
 * 
 * <code>
 * &#64;LogUpadate(describeOperation="ATUALIZA��O", getObjectFrom="cliente")<br>
 * </code>
 * 
 * @param describeOperation Texto que ser� gravado no campo <code>TIPO_OPERACAO</code>. 
 * @param getObjectFrom Nome do identificador que ser� usado para recuperar o objeto original.
 * 
 * @author rlbatista
 *
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface LogUpdate {
	
	/**
	 * Define o texto que deve ser enviado para o atributo <code>'Nome Opera��o'</code> do {@link ObjectLog}.
	 */
	String describeOperation() default "UPDATE";
	
	/**
	 * Indica o nome do identificador usado pela anota��o {@link SendToLogUpdate} para armazenar o objeto original.
	 */
	String getObjectFrom();
}
