package br.com.pst.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import br.com.pst.enums.OnUpdateOperations;
import br.com.pst.model.ObjectLog;

/**
 * Marca qual atributo deve ser logado.<br>
 * Possui o atributo <code>fieldName</code>, que define o texto que ser� enviado para o atributo <code>Nome Coluna</code> de {@link ObjectLog}.
 * Se n�o for informado, ser� enviado o nome do atributo.<br>
 * Ex.: <br><br>
 * 
 * <code>
 * &#64;LogField<br>
 * private Long id;<br>
 * </code>
 * 
 * <i>Envia para o atributo <code>'Nome Coluna'</code> de {@link ObjectLog} o texto 'id'.</i><br><br>
 * 
 * <code>
 * &#64;LogField(fieldName="COD_CLI")<br>
 * private Long id;<br>
 * </code>
 * 
 * <i>Envia para o atributo <code>'Nome Coluna'</code> de {@link ObjectLog} o texto 'COD_CLI'.</i><br><br>
 * 
 * Em opera��es de UPDATE, antes de gerar os dados de log, por padr�o o framework verifica se houve alguma 
 * altera��o no campo anotado e, em caso negativo, o campo � ignorado. Caso queira logar o atributo, independentemente
 * de ter ocorrido altera��o ou n�o, use o atributo <code>'whenLogOnUpdate'</code>. Esse parametro usa um enum para representar
 * essa necessidade: {@link OnUpdateOperations#LOG_ONLY_CHANGED} (default), ou seja, s� loga se houver altera��es
 * no campo e {@link OnUpdateOperations#LOG_ALWAYS} que, instrui o framework a sempre logar o atributo.<br>
 * Ex:<br><br>
 * 
 * <code>
 * &#64;LogField(whenLogOnUpdate=OnUpdateOperations.LOG_ALWAYS)<br>
 * private String nome;<br>
 * </code>
 * <i>Sempre ir� logar o nome em opera��es de UPDATE, mesmo que n�o tenha sido alterado.</i><br>
 * 
 * @param fieldName
 * 		Nome campo que ser� enviado para o atributo <code>'Nome Coluna'</code> de {@link ObjectLog}.
 * @param whenLogOnUpdate
 * 		Define quando o atributo deve ser logado em opera��es de UPDATE (sempre - {@link OnUpdateOperations#LOG_ALWAYS},
 *  	ou somente quando detectada altera��es - {@link OnUpdateOperations#LOG_ONLY_CHANGED}).
 * 
 * @author rlbatista
 *
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface LogField {
	/**
	 * Define o nome da coluna que aparecer� na tabela de log.<br>
	 * Se n�o informado, ser� usado o nome do atributo.
	 */
	String fieldName() default "";
	
	/**
	 * Instrui a opera��o de update se o campo sempre deve ser logado ou somente quando alterado
	 */
	OnUpdateOperations whenLogOnUpdate() default OnUpdateOperations.LOG_ONLY_CHANGED;
}
