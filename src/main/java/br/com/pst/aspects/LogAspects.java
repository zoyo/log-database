package br.com.pst.aspects;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import br.com.pst.annotations.LogDelete;
import br.com.pst.annotations.LogField;
import br.com.pst.annotations.LogInsert;
import br.com.pst.annotations.LogUpdate;
import br.com.pst.annotations.Loggable;
import br.com.pst.annotations.SendToLogUpdate;
import br.com.pst.interfaces.LogDao;
import br.com.pst.interfaces.impl.DefaultLogDao;
import br.com.pst.model.ObjectLog;

/**
 * Classe que implementa os aspectos de log.<br>
 * Possui basicamente as m�todos para interceptar opera��es de 
 * Insert, Update e Delete.
 * 
 * @author rlbatista
 *
 */
@Component("logAspects")
@Scope("prototype")
@Aspect
public class LogAspects {
	private Logger logger = LogManager.getLogger(LogAspects.class);
	
	private Map<String, Object> registers = new HashMap<String, Object>();
	
	/**
	 * Interface respons�vel por processar os dados de log encontrados.
	 */
	private LogDao logDao;
	
	/**
	 * Objeto que representa um objeto de log.
	 */
	private ObjectLog objectLog;
	
	/**
	 * Injeta o contexto do spring. Usado para buscar a implementa��o da interface {@link LogDao}
	 */
	@Autowired
	private ApplicationContext springContext;
	
	/**
	 * A inicializa��o desta classe busca no contexto do Spring dois Beans essenciais para a aplica��o:<br>
	 * A implementa��o de {@link LogDao} (ver {@link #searchLogDaoImplementationOnSprignContext()}) 
	 * e o objeto que representa os dados capturados pelas opera��es e que
	 * ser�o repassados ao m�todo de {@link LogDao} (ver {@link #seachObjectLogOnSpringContext()}) 
	 * que deve ser um objeto do tipo {@link ObjectLog} ou subclasse deste.
	 */
	@PostConstruct
	public void init() {
		logDao = searchLogDaoImplementationOnSprignContext();
		objectLog = seachObjectLogOnSpringContext();
	}

	/**
	 * Busca no contexto do spring, uma implementa��o de {@link LogDao}.
	 *
	 * @return Implementa��o de {@link LogDao} se decladara no contexto do spring ou a implementa��o padr�o {@link DefaultLogDao}
	 */
	private LogDao searchLogDaoImplementationOnSprignContext() {
		LogDao ld = null;
		try {
			ld = springContext.getBean(LogDao.class);
		} catch (BeansException be) {
			ld = new DefaultLogDao();
		}
		
		return ld;
	}
	
	/**
	 * Busca no contexto do spring, o objeto que ser� usado para representar o Log.
	 * 
	 * @return Objeto que representar� o log ou o default {@link ObjectLog}.
	 * 
	 * @throws IllegalArgumentException Se o objeto de log definido pelo cliente n�o extender {@link ObjectLog}.
	 */
	private ObjectLog seachObjectLogOnSpringContext() {
		ObjectLog ol = null;
		try {
			ol = springContext.getBean(ObjectLog.class);
			
			if(!(ol instanceof ObjectLog)) {
				throw new IllegalArgumentException(ol.getClass().getName() +  " n�o � um objeto de log v�lido.");
			}
			
		} catch (BeansException e) {
			ol = new ObjectLog();
		}
		
		
		return ol;
	}
	
	/**
	 * Executa o log de opera��es de INSERT.
	 * 
	 * @param insertAnnotation Anota��o {@link LogInsert} interceptada.
	 * @param savedObject Objeto retornado pelo m�todo de INSERT.
	 */
	@AfterReturning(value="@annotation(insertAnnotation)", returning="savedObject")
	public void doAfterInsert(LogInsert insertAnnotation, Object savedObject) {
		logger.trace("M�todo de INSERT interceptado.");
		
		List<? extends ObjectLog> objectsToLog = buildInfoToLog(savedObject, insertAnnotation);
		logDao.processLogs(objectsToLog);
		
	}
	
	/**
	 * Executa o log de opera��es de UPDATE.
	 * 
	 * @param updateAnnotation Anota��o {@link LogUpdate} interceptada.
	 * @param updatedObject Objeto retornado pelo m�todo de UPDATE. 
	 */
	@AfterReturning(value="@annotation(updateAnnotation)", returning="updatedObject")
	public void doAfterUpdate(LogUpdate updateAnnotation, Object updatedObject) {
		logger.trace("M�todo de UPDATE interceptado");
		
		List<? extends ObjectLog> objectsToLog = buildInfoToLog(updatedObject, updateAnnotation);
		logDao.processLogs(objectsToLog);
	}
	
	/**
	 * Executa o log de opera��es de DELETE.
	 * 
	 * @param deleteAnnotation
	 * @param deletedObject
	 */
	@AfterReturning(value="@annotation(deleteAnnotation)", returning="deletedObject")
	public void doAfterDelete(LogDelete deleteAnnotation, Object deletedObject){
		logger.trace("M�todo de DELETE interceptado.");
		
		List<? extends ObjectLog> objectsToLog = buildInfoToLog(deletedObject, deleteAnnotation);
		logDao.processLogs(objectsToLog);
	}
	
	/**
	 * Obtem o objeto original para enviar ao log de update.<br>
	 * O objeto necessita de um construtor padr�o sem parametros para que seja realizado o 
	 * clone do mesmo via reflection.
	 * 
	 * @param joinPoint Ponto de Jun�ao do Spring AOP.
	 * @param sendToLogUpdateAnnotation Anota��o {@link SendToLogUpdate} interceptada.
	 * @param originalValue Valor retornado pelo m�todo interceptado.
	 * 
	 * @throws Exception Exce��es lan�adas por {@link Class#newInstance()}.
	 */
	@AfterReturning(value="@annotation(sendToLogUpdateAnnotation)", returning="originalValue")
	public void registerOriginalObjectToUpdate(JoinPoint joinPoint, SendToLogUpdate sendToLogUpdateAnnotation, Object originalValue) throws Exception {
		logger.trace("M�tdo de captura de objeto original interceptado.");
		
		String keyToObject = sendToLogUpdateAnnotation.putObjectIn();
		Object newInstance = originalValue.getClass().newInstance();
		BeanUtils.copyProperties(newInstance, originalValue);
		
		registers.put(keyToObject, newInstance);
	}
	
	/**
	 * Extrai do objeto informado os atributos anotados com {@link LogField}.<br>
	 *
	 * @param paramToLog Objeto que ser� inspecionado.
	 * 
	 * @return Lista dos campos que possuem a anota��o {@link LogField}.
	 * 
	 * @throws IllegalArgumentException Se nenhum atributo possuir a anota��o {@link LogField}.
	 */
	private List<Field> extractFieldsToLogFrom(Object paramToLog) {
		logger.trace("Verificando quais campos devem ser logados.");
		
		List<Field> fieldsToLog = new ArrayList<Field>();
		
		Field[] declaredFields = paramToLog.getClass().getDeclaredFields();
		for (Field f : declaredFields) {
			if(f.isAnnotationPresent(LogField.class)) {
				fieldsToLog.add(f);
			}
		}
		
		if(fieldsToLog.isEmpty()) {
			throw new IllegalArgumentException("N�o foi encotrado nenhum campo com a anota��o @LogField.");
		}
		
		return fieldsToLog;
	}

	/**
	 * Extrai do objeto informado, o nome da tabela que deve ser enviado ao 
	 * atributo <code>'Nome Tabela'</code> de {@link ObjectLog}.
	 * 
	 * @param obj Objeto usado para recupera��o do nome da tabela a ser logada.
	 * 
	 * @return Nome da tabela retirado de {@link Loggable} do objeto informado 
	 * atrav�s da chamada a {@link Loggable#tableName()} que, se n�o for preenchido, 
	 * retorna o nome simples da classe.
	 */
	private String getTableNameFromLoggableObject(Object obj) {
		logger.trace("Extraindo o nome da tabela a ser logada.");
		
		String tableName = "";
		Loggable loggable = obj.getClass().getAnnotation(Loggable.class);
		
		tableName = loggable.tableName();
		if (tableName.isEmpty()) {
			tableName = obj.getClass().getSimpleName();
		}
		
		return tableName;
	}
	
	/**
	 * Cria a lista de objetos a logar baseados na instancia do parametro interceptado e na anota��o informada.
	 * 
	 * @param instaceObjectToLog Instancia do objeto que ser� logado.
	 * @param annotation anota��o que vai definir o preenchimento que deve ser realizado.
	 * 
	 * @return Lista dos objetos que devem ser logados.
	 * 
	 * @throws RuntimeException Excess�o lan�ada por {@link #getNewInstanceOfObjectLog()}.
	 * @throws IllegalArgumentException Se <code>'instanceObjectToLog'</code> n�o possuir a anota��o {@link Loggable}
	 */
	private List<? extends ObjectLog> buildInfoToLog(Object instaceObjectToLog, Annotation annotation) {
		logger.trace("Criando as linhas de log.");
		
		if(!instaceObjectToLog.getClass().isAnnotationPresent(Loggable.class)) {
			throw new IllegalArgumentException("O objeto a ser logado n�o est� anotado com br.com.pst.annotations.Loggable");
		}
		
		List<ObjectLog> loggableLines = new ArrayList<ObjectLog>();
		
		List<Field> fieldsToLog = extractFieldsToLogFrom(instaceObjectToLog);
		
		String tableName = getTableNameFromLoggableObject(instaceObjectToLog);
		String contentOfIdentity = getIdentityContentFrom(instaceObjectToLog);
		Date hoje = new Date();
		
		logger.trace("Lendo os atributos para montagem do log.");
		for (Field f : fieldsToLog) {
			String fieldName = getNameFromField(f);
			Object content = getContentFromFieldInInstance(f, instaceObjectToLog);
			
			ObjectLog log = getNewInstanceOfObjectLog();

			log.setNomeTabela(tableName);
			log.setNomeColuna(fieldName);
			log.setIdRegistro(contentOfIdentity);
			log.setDataMoficacao(hoje);
			
			String nomeOperacao = null;
			String valorAntigo = null;
			String valorNovo = null;
			
			if(annotation.annotationType().getCanonicalName().equals(br.com.pst.annotations.LogInsert.class.getCanonicalName())) {
				logger.trace("Montando linha para opera��o de INSERT.");
				
				nomeOperacao = ((LogInsert) annotation).describeOperation();
				valorNovo = content != null ? content.toString() : null;
				
			} else if(annotation.annotationType().getCanonicalName().equals(br.com.pst.annotations.LogUpdate.class.getCanonicalName())) {
				logger.trace("Montando linha para opera��o de UPDATE.");
				
				LogUpdate logUpdateAnnotation = (LogUpdate) annotation;
				nomeOperacao = logUpdateAnnotation.describeOperation();
				
				Object originalObj = registers.get(logUpdateAnnotation.getObjectFrom());				
				Object originalContent = getContentFromFieldInInstance(f, originalObj);				
				
				LogField logFieldAnnotation = f.getAnnotation(LogField.class);				
				boolean shouldLogField = hasChanged(originalContent, content) || logFieldAnnotation.whenLogOnUpdate().isLogAlways();
				
				if(shouldLogField) {
					valorAntigo = (originalContent != null ? originalContent.toString() : null);
					valorNovo = (content != null ? content.toString() : null);
				} else {
					// n�o houve altera��o, ent�o n�o precisa logar.
					continue;
				}
			} else if(annotation.annotationType().getCanonicalName().equals(br.com.pst.annotations.LogDelete.class.getCanonicalName())) {
				logger.trace("Montando linha para opera��o de DELETE.");
				
				nomeOperacao = ((LogDelete) annotation).describeOperation();
				valorAntigo = content != null ? content.toString() : null;
			}

			log.setNomeOperacao(nomeOperacao);
			log.setValorAntigo(valorAntigo);
			log.setValorNovo(valorNovo);
			
			loggableLines.add(log);
		}
		
		return loggableLines;
	}

	/**
	 * Busca o conte�do do campo definido como <code>'identity'</code>
	 * (atrav�s de {@link Loggable#identity()}) no objeto informado.
	 *   
	 * @param instaceObjectToLog Objeto onde o campo ser� localizado.
	 * @return Conte�do do campo informado ou uma string vazia caso o 
	 * 		   valor seja null ou o atributo n�o seja encontrado.
	 */
	private String getIdentityContentFrom(Object instaceObjectToLog) {
		Loggable loggableAnnotation = instaceObjectToLog.getClass().getAnnotation(Loggable.class);
		String fieldNameIdentity = loggableAnnotation.identity();
		String contentIdentity = "";
		
		try {
			Field field = instaceObjectToLog.getClass().getDeclaredField(fieldNameIdentity);
			Object content = getContentFromFieldInInstance(field, instaceObjectToLog);
			
			if(content != null) {
				contentIdentity = content.toString();
			}
			
		} catch (Exception e) {
			throw new RuntimeException(String.format("Identificador [%s] n�o foi encontrado em [%s]", contentIdentity, instaceObjectToLog.getClass().toString()), e);
		}
		
		return contentIdentity;
	}

	/**
	 * Cria uma nova istancia do objeto de log.
	 * 
	 * @return Instacia construida �partir do bean definido pelo usu�rio ou do padr�o {@link ObjectLog}.
	 * 
	 * @throws RuntimeException As exceptions lan�adas por {@link Class#newInstance()} encapsuladas nesta.
	 */
	private ObjectLog getNewInstanceOfObjectLog()  {
		ObjectLog newInstance = null;
		
		try {
			newInstance = objectLog.getClass().newInstance();
			
		} catch (Exception e) {
			throw new RuntimeException("Erro ao construir objeto de log. Verifique se o mesmo possui um construtor padr�o sem par�metros.", e);
		}
		
		return newInstance;
	}

	/**
	 * Verifica se houve altera��o no objeto orignal.<br>
	 * A verifica��o usa o conte�do do toString e os compara com o equalsIgnoreCase.
	 * 
	 * @param originalContent Objeto original capturado pela anota��o {@link SendToLogUpdate}
	 * @param newContent conte�do atual do campo capturado pela anota��o {@link LogUpdate}
	 * 
	 * @return true se houve altera��o no campo ou false caso contr�rio.
	 */
	private boolean hasChanged(Object originalContent, Object newContent) {
		if(originalContent == null && newContent == null) {
			return false;
			
		} else if((originalContent == null && newContent != null) || (originalContent != null && newContent == null)) {
			return true;
			
		} else {
			return !originalContent.toString().equalsIgnoreCase(newContent.toString());
		}
	}

	/**
	 * Extrai do campo informado o nome de coluna que ser� enviado para o atributo
	 * <code>'Nome Coluna'</code> de {@link ObjectLog}.
	 * 
	 * @param f Campo que ser� verificado.
	 * 
	 * @return Nome da coluna retornado pela anota��o {@link LogField} ou o pr�prio nome do atributo
	 * caso este n�o tenha sido informado na anota��o.
	 */
	private String getNameFromField(Field f) {
		logger.trace("Extraindo o nome do atributo.");
		
		LogField logField = f.getAnnotation(LogField.class);
		
		String fieldName = logField.fieldName(); // Obt�m o nome do campo
		
		if(fieldName.isEmpty()) {
			fieldName = f.getName();
		}
		
		return fieldName;
	}

	/**
	 * Extrai o conte�do do campo <code>f</code> da instancia <code>instance</code>.
	 * 
	 * @param f Campo que ser� verificado.
	 * @param instace Instancia que possui o campo <code>f</code>.
	 * 
	 * @return O conte�do do campo informado ou uma string vazia em caso de algum erro no acesso ao campo.
	 * 
	 * @throws IllegalArgumentException Se o campo informado n�o pertencer a instancia informada.
	 */
	private Object getContentFromFieldInInstance(Field f, Object instace) {
		logger.trace("Extraindo o conte�do do campo.");
		
		Object ret = null;
		
		
		try {
			boolean originalAccessible = f.isAccessible();
			f.setAccessible(true);
			ret = f.get(instace);
			f.setAccessible(originalAccessible);
			
		} catch (IllegalArgumentException e) {
			throw new IllegalArgumentException(String.format("O campo [%s] n�o pertence a instancia informada.", f.getName()), e);
			
		} catch (IllegalAccessException e) {
			ret = "";
		}
		
		return ret;
	}
	
	/**
	 * Setter da interface {@link LogDao}. Permite a inje��o por setter.
	 * @param logDao Implementa��o {@link LogDao} que ser� injetada para gerenciamento dos logs.
	 */
	public void setLogDao(LogDao logDao) {
		this.logDao = logDao;
	}
}
