package br.com.pst.interfaces;

import java.util.List;

import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import br.com.pst.model.ObjectLog;

/**
 * Provê uma interface de callback para manipula��o dos logs capturados.<br>
 * Essa interface � injetada na classe que manipula os logs, portanto,
 * basta adicionar um bean no arquivo de configura��o do spring ou anotar
 * a classe com umas das anota��es do spring ({@link Component}, {@link Service}, {@link Repository}, etc...). 
 * 
 * @author rlbatista
 *
 */
public interface LogDao {
	/**
	 * m�todo de callback que ser� chamado nas opera��es de log.
	 * @param objectsLog Objetos de log detectados que ser�o precessados (salvos em banco, arquivo, etc...).
	 */
	void processLogs(List<? extends ObjectLog> objectsLog);
}
