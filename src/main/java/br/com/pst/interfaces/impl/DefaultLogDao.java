package br.com.pst.interfaces.impl;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import br.com.pst.interfaces.LogDao;
import br.com.pst.model.ObjectLog;

/**
 * Implementa��o padr�o da interface respons�vel por processar os dados de log encontrados.<br>
 * Caso a aplica��o cliente n�o implemente, esta ser� usada.<br>
 * Essa implementa��o padr�o apenas loga os dados usando o Log4j
 * atrav�s do m�todo <code>info</code>.  
 * 
 * @author rlbatista
 *
 */
public class DefaultLogDao implements LogDao {
	private Logger logger = LogManager.getLogger(DefaultLogDao.class);

	@Override
	public void processLogs(List<? extends ObjectLog> objectsLog) {
		for (ObjectLog ol : objectsLog) {
			logger.info(ol);
		}
	}
}
