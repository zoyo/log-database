package br.com.pst.enums;

import br.com.pst.annotations.LogField;

/**
 * Usado na anota��o {@link LogField} e serve para instruir o framework, durante
 * opera��es de UPDATE, se o atributo anotado deve sempre ser logado, ou s� quando
 * altera��es forem detectadas.
 *  
 * @author rlbatista
 *
 */
public enum OnUpdateOperations {
	/**
	 * Sempre loga o atributo anotado.
	 */
	LOG_ALWAYS(true),
	
	/**
	 * Loga somente quando forem detectadas altera��es.
	 */
	LOG_ONLY_CHANGED(false);
	
	private boolean logAlways;

	private OnUpdateOperations(boolean logAlways) {
		this.logAlways = logAlways;
	}
	
	public boolean isLogAlways() {
		return logAlways;
	}
}
