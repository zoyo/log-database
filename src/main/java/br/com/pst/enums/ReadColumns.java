package br.com.pst.enums;

import br.com.pst.annotations.LogField;

// TODO O enum ainda n�o est� sendo usado. Implementar futuramente essa id�ia de defini��o do que deve ser logado. 
/**
 * Define quais atributos devem ser lidos para realiza��o do log.<br>
 * H� 3 op��es de leitura dos atributos:<br>
 * 
 * <table>
 * 		<tr>
 * 			<td><code>ALL</code></td>
 * 			<td>Verifica todos os atributos da classe</td>
 * 		</tr>
 * 		<tr>
 * 			<td><code>ONLY_ANNOTATED</code></td>
 * 			<td>Verifica somente atributos anotados com {@link LogField}</td>
 * 		</tr>
 * 		<tr>
 * 			<td><code>NOT_ANNOTATED</td>
 * 			<td>Verifica todos atributos que NÃO possuem a anota��o {@link LogField}</td>
 * 		</tr>
 * </table>
 * 
 * @author rlbatista
 *
 */
public enum ReadColumns {
	/**
	 * Todos os atributos da classe ser�o logados.
	 */
	ALL,
	
	/**
	 * Somente os atributos da classe anotados com &#64;LogField ser�o logados.
	 */
	ONLY_ANNOTATED,
	
	/**
	 * Atributos anotados com &#64;NotLog ser�o ignorados.
	 */
	NOT_ANNOTATED
}
