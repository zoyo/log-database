package br.com.pst.model;

import java.util.Date;

import br.com.pst.annotations.LogDelete;
import br.com.pst.annotations.LogField;
import br.com.pst.annotations.LogInsert;
import br.com.pst.annotations.LogUpdate;
import br.com.pst.annotations.Loggable;
import br.com.pst.interfaces.LogDao;

/**
 * Classe usada para representa um item de log.<br>
 * A classe permite extens�o caso queira adicionar outras informa��es ao log, como por exemplo, 
 * o nome do usu�rio que efetuou as altera��es, ou IP de origem da m�quina cliente.<br>
 * Para isso, extenda essa classe e adicione as informa��es necess�rias e, na implementa��o do
 * {@link LogDao} defina as informa��es adicionadas.<br> 
 * 
 * Esta classe possui os seguintes atributos:<br><br>
 * 
 * <table border=1>
 * 		<tr>
 * 			<th>Atributo</th>
 * 			<th>Descri��o</th>
 * 		</tr>
 * 		<tr>
 * 			<td>id</td>
 * 			<td>
 * 				Identificador da tabela de log. Este atributo n�o � preenchido pelo framework. Pode ser usado,
 * 				por exemplo, como chave prim�ria do objeto de log para envio a um banco de dados.
 * 			</td>
 * 		</tr>
 * 		<tr>
 * 			<td>Nome Opera��o</td>
 * 			<td>
 * 				Nome da opera��o que est� sendo executada. Possui os valores padr�o: "INSERT", "UPDATE" ou "DELETE".
 * 				Esses valores vem das anota��es {@link LogInsert}, {@link LogUpdate} ou {@link LogDelete} respectivamente
 * 				atrav�s da chamada ao m�todo describeOperation() de cada uma.
 * 			</td>
 * 		</tr>
 * 		<tr>
 * 			<td>Nome Tabela</td>
 * 			<td>
 * 				Nome da tabela que est� sendo modificada. O nome vem da anota��o {@link Loggable} atrav�s da chamada ao
 * 				tableName(), que, se n�o informado, ser� usado o nome da pr�pria classe.
 * 			</td>
 * 		</tr>
 * 		<tr>
 * 			<td>ID Registro</td>
 * 			<td>
 * 				Identificador do registro que sofreu a altera��o. Deve ser definido atrav�s de {@link Loggable#identity()}
 * 				que deve marcar na classe monitorada, o campo que representa a chave prim�ria, por exemplo.
 * 			</td>
 * 		</tr>
 * 		<tr>
 * 			<td>Nome Coluna</td>
 * 			<td>
 * 				Nome da coluna que foi alterada. O nome vem da anota��o {@link LogField} atrav�s da chamdada ao m�todo
 * 				fieldName(), que, se n�o informado, ser� usado o pr�prio nome do atributo.
 * 			</td>
 * 		</tr>
 * 		<tr>
 * 			<td>Valor Antigo</td>
 * 			<td>Antigo valor da coluna que est� sendo logada. Preenchida nas opera��es de UPDATE e DELETE.</td>
 * 		</tr>
 * 		<tr>
 * 			<td>Valor Novo</td>
 * 			<td>Novo valor da coluna que est� sendo logada. Preenchida nas opera��os de UPDATE e INSERT.</td>
 * 		</tr>
 * 		<tr>
 * 			<td>Data Modifica��o</td>
 * 			<td>Data em que ocorreu a opera��o</td>
 * 		</tr>
 * </table>
 * 
 * @author rlbatista
 *
 */
public class ObjectLog {
	private Long id;
	private String nomeOperacao;
	private String nomeTabela;
	private String idRegistro;
	private String nomeColuna;
	private String valorAntigo;
	private String valorNovo;
	private Date dataMoficacao;
	
	@Override
	public String toString() {
		StringBuilder str = new StringBuilder();
		str.append("%s")
		   .append(" [")
		   .append("id=%d").append(", ")
		   .append("nomeOperacao=%s").append(", ")
		   .append("nomeTabela=%s").append(", ")
		   .append("idRegistro=%s").append(", ")
		   .append("nomeColuna=%s").append(", ")
		   .append("valorAntigo=%s").append(", ")
		   .append("valorNovo=%s").append(", ")
		   .append("dataMoficacao=%tc")
		   .append("]");
		
		
		return String.format(str.toString(), 
								"ObjectLog",
								this.getId(),
								this.getNomeOperacao(),
								this.getNomeTabela(),
								this.getIdRegistro(),
								this.getNomeColuna(),
								this.getValorAntigo(),
								this.getValorNovo(),
								this.getDataMoficacao());
	}

	/*
	 * GETTERS/SETTERS
	 */
	public final Long getId() {
		return id;
	}

	public final void setId(Long id) {
		this.id = id;
	}

	public final String getNomeOperacao() {
		return nomeOperacao;
	}

	public final void setNomeOperacao(String nomeOperacao) {
		this.nomeOperacao = nomeOperacao;
	}

	public final String getNomeTabela() {
		return nomeTabela;
	}

	public final void setNomeTabela(String nomeTabela) {
		this.nomeTabela = nomeTabela;
	}
	
	public final String getIdRegistro() {
		return idRegistro;
	}
	
	public void setIdRegistro(String idRegistro) {
		this.idRegistro = idRegistro;
	}
	public final String getNomeColuna() {
		return nomeColuna;
	}

	public final void setNomeColuna(String nomeColuna) {
		this.nomeColuna = nomeColuna;
	}

	public final String getValorAntigo() {
		return valorAntigo;
	}

	public final void setValorAntigo(String valorAntigo) {
		this.valorAntigo = valorAntigo;
	}

	public final String getValorNovo() {
		return valorNovo;
	}

	public final void setValorNovo(String valorNovo) {
		this.valorNovo = valorNovo;
	}

	public final Date getDataMoficacao() {
		return dataMoficacao;
	}

	public final void setDataMoficacao(Date dataMoficacao) {
		this.dataMoficacao = dataMoficacao;
	}
}
